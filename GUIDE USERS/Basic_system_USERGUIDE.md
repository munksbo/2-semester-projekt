# USER GUIDE

## Before you start

Please read through the userguide before you begin, and then take it step by step.
before you can begin, have a formatet SD card prepared with *stretch* installed.


## Setting up the basic system
Connect the atmega and the raspberry like this [schematic][schematic]

## Setting up the RPI
To get the instalation script <br >
type ```wget https://gitlab.com/munksbo/2-semester-projekt/-/archive/master/2-semester-projekt-master.zip```

to unzip the folder <br >
type ```unzip 2-semester-projekt-master.zip```

Change directory to the script <br >
type ```cd /home/pi/Code/Auto_installer```

Make the script executeable <br >
type ```sudo chmod 755 updater_v1.sh```

And run the script <br >
type ```sudo ./updater_v1.sh```
## Router configuration
To setup the router follw [this][router] guide 


[schematic]: https://gitlab.com/munksbo/2-semester-projekt/blob/master/Documentation./Designs/minimal_system_schematic.pdf
[router]: https://gitlab.com/munksbo/2-semester-projekt/blob/master/GUIDE%20USERS/Network-Router_config.md