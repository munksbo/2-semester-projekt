# Network - Router configuration.

1. To set up access via ssh you need to configure the users. To set a user use this configuration. 

```
system {
    login {
        }
        user <insert username> {
            class super-user;
            authentication {
                ssh-rsa "<insert your public ssh key>"
            }
        }
        
    }
```
2. To get it more secure with ssh keys it is a good idea to deny root access via ssh with this config.

```
system{
    services {
        ssh {
            root-login deny-password;
            }
        }
    }
``` 
    
3. The dhcp server is set up like this.

```
system {
    services {
        dhcp {
            pool 192.168.10.0/24 {
                address-range low 192.168.10.50 high 192.168.10.100;
                name-server {
                    1.1.1.1;
                }
                router {
                    192.168.10.1;
                    }
                }
            }
        }
    }
```

4. The next thing is to set up a vlan with a layer 3 interface.

```
vlans {
    vlan-30 {
        vlan-id 30;
        l3-interface vlan.30;
        }
    }
```

5. next up you set the interfaces to be a member of the vlan you created.

```
interfaces {
    ge-0/0/11 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-30;
                }
            }
        }
    }
    ge-0/0/12 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-30;
                }
            }
        }
    }
    ge-0/0/13 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-30;
                }
            }
        }
    }
    ge-0/0/14 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-30;
                }
            }
        }
    }
}
```

6. Then you set up the layer 3 address for the vlan.

```
interfaces {
    vlan {
        unit 30 {
            family inet {
                address 192.168.30.1/24;
            }
        }
    }
}
```

7. To get access to the internet you need one static route wich is the deafult route.

```
routing-options {
    static {
        route 0.0.0.0/0 next-hop 10.217.16.1;
    }
}
```

8. The natting is set up like this.

```
security {
    nat {
        source {
            rule-set trust-to-untrust {
                from zone trust;
                to zone untrust;
                rule source-nat-rule {
                    match {
                        source-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
        }
    }
}
```

9. To specify the destination to an ip address you use destination natting like this.
With this configuration can we access the same server on 3 different applications.

```
security {
    nat {
        destination {
            pool ssh-grp-30 {
                address 192.168.30.10/32 port 22;
            }
            pool http-grp-30 {
                address 192.168.30.10/32 port 80;
            }
            pool api-grp-30 {
                address 192.168.30.10/32 port 5000;
            }
            rule-set rs1-vlan-30 {
                from interface ge-0/0/15.0;
                rule r1 {
                    match {
                        destination-address 10.217.19.211/32;
                        destination-port 1300;
                    }
                    then {
                        destination-nat {
                            pool {
                                des-nat-pool-vlan-30;
                            }
                        }
                    }
                }
                rule http-grp-30 {
                    match {
                        destination-address 10.217.19.211/32;
                        destination-port 3000;
                    }
                    then {
                        destination-nat {
                            pool {
                                http-grp-30;
                            }
                        }
                    }
                }
                rule api-grp-30 {
                    match {
                        destination-address 10.217.19.211/32;
                        destination-port 4300;
                    }
                    then {
                        destination-nat {
                            pool {
                                api-grp-30;
                            }
                        }
                    }
                }
            }
        }
    }
}
```

11. To allow data flow between zones you need policies like this.

```
security {    
    policies {
        from-zone trust to-zone untrust {
            policy trust-to-untrust {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone trust {
            policy untrust-to-trust {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone trust to-zone trust {
            policy trust-to-trust {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
}
```

12. A interface need to be in a security zone all the interfaces we are using are placed in trust zone.

```
security {
    zones {
        security-zone trust {
                ge-0/0/11.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                        protocols {
                            all;
                        }
                    }
                }
                ge-0/0/12.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                    }
                }
                ge-0/0/13.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                        protocols {
                            all;
                        }
                    }
                }
                ge-0/0/14.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                    }
                }
                vlan.30 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                        protocols {
                            all;
                        }
                    }
                }
            }
        }
    }
}
```