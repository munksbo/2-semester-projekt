Group: munksbo
----------------------------

Class: B

Members

* Niclas Andersen ptauhg   
* Mathias Munksbo
* Jesper Aggerholm jesp
* Daniel Maslygan

Juniper router:

* Management SSH: 10.217.19.211:22
* Externatl ip: 10.217.19.211

Raspberry:

* SSH access: 10.217.19.211:1300
* Rest API access: [http://10.217.19.211:4300](http://10.217.19.211:4300) 

Group web server:

* SSH access: 10.217.16.88:22
* Rest API access: [http://10.217.16.88:80](http://10.217.16.88:80)