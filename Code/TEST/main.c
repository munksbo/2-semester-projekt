/*
 * DataLogger.c
 *
 * This example gets or sets port values via commands from UART. 
 * UART is using 9600 BAUD, 8 databits, 1 stop bit, no parity.
 *
 * it reads adc value from from the specified ADC port.
 * It detects button presses on specified GPIO port and pin
 * It manipulates a led on specified GPIO port and pin
 *
 * UART commands:
 *
 * led1on (turns on led1, replies with led1=1)
 * led1off (turns off led1, replies with led1=0)
 * getbtn1 (reads button 1 status, replies with btn1=0 or btn=1, resets status if btn1=1)
 * getadcval (returns raw adc value from ADC)
 *
 * Created: 14-01-2019 12:44:50
 * Modified: 14-02-2019 19:39:33
 * Author : Nikolaj Simonsen
 *
 */ 

// Macros and libraries
#define F_CPU 8000000UL //use 8MHz clock
#define BAUD 9600 //BAUD Rate for UART
#include <stdio.h> // Needed for printf + scanf
#include <avr/io.h> // avr macros
#include "Library/STDIO_UART.h" // Needed for stdio to UART redirection
#include "Library/PORT.h"
#include <stdbool.h> // Needed for bool
#include <avr/interrupt.h> // Needed for interrupts
#include <string.h>

// global variables used in interrupts
volatile bool btn1Pressed = false;
volatile bool UartInput = false;

//constants
const char btn1 = 0b10000000;
const char led1 = 0b00100000;
const char adcinput = 0b00000001; //last 4 bits determines adc input pin, datasheet section 21.9.1, table 21-4

//function prototypes
bool readLed(char led);
void ledOn(char led);
void ledOff(char led);
void toggleLed(char led);


void setupTimer0();
void setupADC(char adcbit);
char startConversion();
void promptReady();

int main(void)
{
	//Initialization
	ioinit(); //Setup UART and STDIO
	setupADC(adcinput);
	initPort(DDRB, led1);
	enablePullup(PORTB, btn1);
	char userinput[32]; //array to hold the userinput
	setupTimer0(); //used for button polling
	sei(); //enable interrupts	
	promptReady();
	
	while (1)
	{					
		if (UartInput)
		{			
			scanf("%30s", userinput); //read userinput
			
			if(strcmp(userinput, "led1on")==0) {
				ledOn(led1);
				printf("led1=%d\n", readLed(led1));				
				userinput[0] = 0;
				promptReady();
			}
			else if(strcmp(userinput, "led1off")==0) {
				ledOff(led1);
				printf("led1=%d\n", readLed(led1));				
				userinput[0] = 0;
				promptReady();
			}			
			else if(strcmp(userinput, "getbtn1")==0) {
				printf("btn1=%d\n", btn1Pressed);				
				btn1Pressed = false;
				userinput[0] = 0;
				promptReady();
			}			
			else if(strcmp(userinput, "getadcval")==0) {
				printf("%d\n", startConversion());				
				userinput[0] = 0;
				promptReady();
			}
			
			else {
				printf("\ninput error: %s\n", userinput);
				promptReady();
			}
			UartInput = false;			
		}
	}
}

//Timer0 interrupt routine to poll for button presses app. every 16ms. 
//TODO: Remove hardcoded pin register
ISR(TIMER0_OVF_vect) {
	if (!(PINB & btn1) && !btn1Pressed) {
		btn1Pressed = true;
	}
}

//UART receive interrupt routine
ISR(USART0_RX_vect) {
	UartInput = true; //set the UART interrupt flag
}

//Timer0 setup
void setupTimer0() {
	TCCR0B |= (1 << CS00) | (1 << CS02); //normal mode, /1024 prescaler, 16ms overflow
	TIMSK0 |= (1 << TOIE0); //timer0 overflow interrupt
}

//TODO: Remove hardcoded pin register ref
//This function reads the LED pin. 
bool readLed(char led) {
	if (!(PINB & led)){ //if led bit is 0 using AND mask
		return false;
	}
	else{
		return true;
	}
}

//TODO: Remove hardcoded port ref
//This function turns on the LED. 
void ledOn(char led) {
	PORTB |= led; //turn on led bit, leave rest with OR mask
}

//TODO: Remove hardcoded port ref
//This function turns off the LED. 
void ledOff(char led) {
	PORTB &=~ led; //turn off led bit, leave rest with AND mask
}

void setupADC(char adcbit) {
	ADMUX = (1 << REFS0) | (adcbit); //use 5V reference voltage, adcbit decides port
	ADCSRA = (1 << ADEN) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2); //enable ADC, ADC prescaler=128 (8000000/128=62.5KHz) (1 << ADIE)
	DIDR0 = (1 << ADC0D); //turn off digital input buffer on pin PORTC0	
	startConversion();
}

char startConversion() {
	cli();
	ADCOut = ADCSRA |= (1 << ADSC); //starts a conversion, cleared when ADC interrupt is fired
	Temperature=ADCOut/4
	sei();
	return Temperature;
}

void promptReady() {
	printf("> ");
};