#!/bin/bash
#welcom
printf "
   _  _   _ _____ ___  __  __   _ _____ ___ ___          ___         _      _
  /_\| | | |_   _/ _ \|  \/  | /_\_   _| __|   \   ___  / __| __ _ _(_)_ __| |_
 / _ \ |_| | | || (_) | |\/| |/ _ \| | | _|| |) | |___| \__ \/ _| '_| | '_ \  _|
/_/ \_\___/  |_| \___/|_|  |_/_/ \_\_| |___|___/        |___/\__|_| |_| .__/\__|
                                                                      |_|
\n"


#stoff
project_link="https://gitlab.com/munksbo/2-semester-projekt/-/archive/master/2-semester-projekt-master.zip"
project_name="2-semester-projekt-master"
flask_api_path="/Flask_api/menu.py"
Allowed_users=(munksbo nicas4100 JesperAggerholm)

#Networking settings
Interface="eth0"
Ip="192.168.30.10/24"
Router_ip="192.168.30.1"
DNS_server="8.8.8.8"

#Add network config for static IP
echo "[ \e[32m* ] Configuring static IP"
echo "" >> /etc/dhcpcd.conf
echo "interface $Interface" >> /etc/dhcpcd.conf
echo "static ip_address=$Ip" >> /etc/dhcpcd.conf
echo "static routers=$Router_ip" >> /etc/dhcpcd.conf
echo "static domain_name_servers=$DNS_server" >> /etc/dhcpcd.conf

#enable SSH
echo "[ \e[32m* ] Enabling SSH."
sudo systemctl enable ssh
sudo systemctl start ssh

#Enable Serial
echo "[ \e[32m* ] Enabling serial ports"
echo "" >> /boot/config.txt
echo "enable_uart=1" >> /boot/config.txt

#Add user's SSH-keys
echo "[ \e[32m* ] Setting up SSH"
sudo mkdir /home/pi/.ssh
touch /home/pi/.ssh/authorized_keys
echo "" > /home/pi/.ssh/authorized_keys

for user in ${Allowed_users[*]}
do
    echo item: Adding User: $user
    curl https://gitlab.com/$user.keys >> /home/pi/.ssh/authorized_keys
done

#install packages
echo "[ \e[32m* ] Install python3-pip"
sudo apt-get -y -q install python3-pip
echo "[ \e[32m* ] Install unzip"
sudo apt-get install unzip
echo "[ \e[32m* ] Install pyserial"
sudo pip3 install pyserial -q
echo "[ \e[32m* ] Install flask"
pip3 install flask -q
pip3 install flask_restful -q
pip3 install flask_cors -q

#Get zip-file
echo "[ \e[32m* ] Downloading projekct"
wget -O $project_name.zip $project_link
#unzip file
unzip -o $project_name.zip


#Flask auto-run
echo "[ \e[32m* ] Setup auto-run Flask"
echo "[Unit]" > /lib/systemd/system/flask_api.service
echo "Description=Flask api service" >> /lib/systemd/system/flask_api.service
echo "[Service]" >> /lib/systemd/system/flask_api.service
echo "Type=static" >> /lib/systemd/system/flask_api.service
echo "ExecStart=/usr/bin/python3 /home/pi/"$project_name$flask_api_path >> /lib/systemd/system/flask_api.service
echo "[Install]" >> /lib/systemd/system/flask_api.service
echo "WantedBy=boot-complete.target" >> /lib/systemd/system/flask_api.service

sudo chmod 644 /lib/systemd/system/flask_api.service

sudo systemctl daemon-reload
sudo systemctl enable flask_api.service

sudo reboot
