# Home watering system

The system will measure the moisture levels will be measured with a moisture sensor. 
It will then check if the levels are within acceptable levels, if not. The system will engage a pump, that will start the dripping system.

Expansion of the system will include sunlight detection, temperature and humidity readings. it will be expanded so it can manage and control 
multiple sensor sticks, with each their own pump dedicated to them.

# Current goals on levels of design.

## Minimum system

This system will only have a moisture sensor and a pump connected, the sensor will detect if the dirt is within acceptable levels. 
If not, it will engagde the pump system. The data will be stored and displayed on our webserver, were the current plan will include a graph
displaying the water levels through the day, the time when the pump will be engaged and maybe the water used. 

## Medium system

As described above we will be adding a tempreture, humidity and sunlight sensor to the sensor stick and expand on the webpage making it both more 
user friendly and maybe look into app functionality. 
A database might be included where we have found the optimal conditions for each specific plant and it's needs.

## Maxium system.

All of the above have been achived and we will fokus on bug fixin, expansion of the plant database and further improvement on the app.
considering a mail system, that will automaticly send a warning if the system should encounter errors.
The maximum system will also give opportunity to adjust the acceptable values for the soil moisture.