# Design ideas

since our design is ment to be located in a greenhouse, we have to consider certain things.
It will be damp, humid, high temperatures and dusty. Therefore we must develop our product 
around these issues.

## Things to consider

### Battery

1. Exposure to high/cold enviroment.
2. Moisture/Humidity.

## ATmega / RPI

1. Exposure to hight/cold enviroment.
2. Moisture/Humidity.
3. Battery life.
4. Lifespan of the devices and wires.

## Possible solutions

1. Consider using two fans for airflow within the box, this will help keep a dry enviroment within.
this will require filters that can be removed from time to time and clear any dust collected.
(any possible way to reduce moisture with filters?)

2. Battery will be designed with it's own little compartment, that will be as airtight/waterproffed.

