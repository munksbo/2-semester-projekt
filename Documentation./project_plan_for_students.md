---
title: '19S ITT2 Project'
subtitle: 'Project plan'
authors: ['Jesper Tinus Aggerholm jesp9795@ucl.dk', 'Niclas Andersen nicl0575@edu.eal.dk', '']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@eal.dk'
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. 
This project will cover the first part of the project, and will match topics that are taught in parallel classes.


# Purpose


The main goal is to have a system where sensor data is send to a cloud server, presented and collected, and data and control signal may be send back also.
This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system
and to give them hands-on experience with the implementation also.

For simplicity, the goals stated wil evolve around the technical project goals.
It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

![project_overview](project_overview.png)

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. 
Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the seralconnection to the raspberry pi.
* Raspberry Pi: Minimal linux system relevant programs to upload/download data from the ATMega and to/from the APIs.
* Juniper router: Router to protect your internal system from the untrusted networks, and to enable access to the Internet and the "cloud servers"
* Reverse proxy/API gateway: This is a server/router that collects and protects the API endpoints and webservers implemented by each group.
* webservers: There are one webserver per group. It will include the REST API implementation and the user interface website.


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* A secure firewall enabling connections to the API.
* Docmentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from now to the easter holidays.

See the lecture plan for details.


# Organization

- Jesper Aggerholm (Project manager) jesp9795@ucl.dk  
- Mathias Munksbo (Programmer)
- Niclas Andersen (Technician)

# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

## Issues related to hardware

- Making sure that we have made a completly sealed main hub and sensor stick.
- Moisture, that ill contamine the hardware.
- ...
- 
## Group related

- Stretch goals
- Time management
-...

# Stakeholders
TBD....

- Teachers 
- (Future investors)
- Friends
- Classmates

# Communication

Weekly communication with the teachers with a 10 miniute meeting.

* agenda
* showcase completet tasks
* issues

# Perspectives

This project will serve as a template for other similar project, ike the second one in ITT2 or in 3rd semester.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. 
Both of which may have consequences on future projects.The will be some documentation needed at the end (or during) the project, 
e.g. external funding will require some sort of reporting after the project has finished. This must be described.]

TBD: Students fill out this one also

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

None at this time
