Recreation checklist for group <other group name>
===================================================

Group name: munksbo

Checklist
------------

- [x] Project plan completed
- [x] Minimal circuit schematic completed
- [x] ATmega328 minimal system code with tempsensor board working
- [x] ATMega328 to Rasperry Pi serial communication over UART working
- [x] Raspberry pi configuration week06 requirements fulfilled
- [x] Networked raspberry pi+atmega system behind a firewall accessible using SSH
- [x] Temperature readout in celcius from the temperature sensor
- [ ] Raspberry API Server is able to serve temperature readings
- [x] Virtual server installed
- [ ] `group.md` complete according to specification
- [ ] Dashbaord communicating with API

Comments
-----------

<Any comments that may be relevant for you, other groups or teachers. (Could be that something is super succesful)>
